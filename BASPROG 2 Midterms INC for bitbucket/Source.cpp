#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

struct playerArsenal
{
	string sideName;
	vector<string> deckNames;
	int deckSize = 5;
	int money = 0;
	int wager = 0;
	int mm = 30;
};

//Displays the players info
void printInfo(playerArsenal& player)
{
	cout << " Kaiji's Cash: " << player.money << endl;
	cout << "MM Left: " << player.mm << endl;
}

// Displays the bet/wager of the player
void getWager(playerArsenal& player)
{
	//This gets the players wager amount
	do
	{
		cout << "How much do you want to wager? " << endl;
		cin >> player.wager;
	} while (player.wager <= 0 || player.wager > player.mm);
}

// This one checks which side is the player and the enemy is on
void deckChecker(playerArsenal& player, playerArsenal& enemy, int& playRound)
{
	if ((playRound >= 1 && playRound <= 3) || (playRound >= 7 && playRound <= 9))
	{
		player.sideName = "Emperor Card ";
		player.deckNames.push_back("Emperor Card! ");
		for (int i = 0; i < 4; i++)
		{
			player.deckNames.push_back("Citizen ");
		}

		enemy.sideName = "Slayer";
		enemy.deckNames.push_back("Slave");
		for (int i = 0; i < 4; i++)
		{
			enemy.deckNames.push_back("Citizen");
		}
	}

	else if ((playRound >= 4 && playRound <= 6 || (playRound >= 10 && playRound <= 12)))
	{
		player.sideName = "Emperor Card!";
		player.deckNames.push_back("Emperor Card!");
		for (int i = 0; i < 4; i++)
		{
			player.deckNames.push_back("Citizen");
		}

		enemy.sideName = "Slave Card!";
		enemy.deckNames.push_back("Slave Card!");
		for (int i = 0; i < 4; i++)
		{
			enemy.deckNames.push_back("Citizen");
		}
	}

}

// This displays the side that the player is on and the players deck/cards
void printDeck(playerArsenal& player)
{
	cout << "Kaiji's Side: " << player.sideName << endl;
	for (int i = 0; i < player.deckSize; i++)
	{
		cout << "[" << i + 1 << "]" << player.deckNames[i] << endl;
	}
}

//This shows the Player and the enemy's cards and side
void allDeck(playerArsenal& player, playerArsenal& enemy, int& playRound)
{
	if ((playRound >= 1 && playRound <= 3) || (playRound >= 7 && playRound <= 9))
	{
		player.sideName = "Emperor";
		player.deckNames.push_back("Emperor");
		for (int i = 0; i < 4; i++)
		{
			player.deckNames.push_back("Citizen");
		}

		enemy.sideName = "Slave";
		enemy.deckNames.push_back("Slave");
		for (int i = 0; i < 4; i++)
		{
			enemy.deckNames.push_back("Citizen ");
		}

	}

	else if ((playRound >= 4 && playRound <= 6 || (playRound >= 10 && playRound <= 12)))
	{
		player.sideName = "Slave";
		player.deckNames.push_back("Slave");
		for (int i = 0; i < 4; i++)
		{
			player.deckNames.push_back("Citizen");
		}

		enemy.sideName = ("Emperor");
		enemy.deckNames.push_back("Emperor");
		for (int i = 0; i < 4; i++)
		{
			enemy.deckNames.push_back("Citizen");
		}

	}
}

//This is the Gameround 
void playRound(playerArsenal& player, playerArsenal& enemy, int& choice)
{
	int enemyChoice = rand() % 5 + 1;

	if (player.sideName == "Emperor")
	{
		
		if (choice == 1)
		{
			cout << "Kaiji has DRAWN THE EMPEROR CARD!!! " << endl;
			if (enemyChoice > 1)
			{
				cout << "Tonegawa has drawn a Civillian Card!" << endl;
				cout << "KAIJI WON!!! " << player.wager * 100000 << "YENNIEEEE!!! " << endl;
				player.money += (player.wager * 100000);
			}

			else
			{
				cout << "Tonegawa has drawn the SLAVE CARD!" << endl;
				cout << "Kaiji Lost! OH NO! THE DRILL STARTS MOVING!!!" << player.wager << "MM!!!!!" << endl;
				player.mm -= player.wager;
			}
		}

		else
		{
			cout << "Kaiji has drawn the Civillian card!" << endl;

			if (enemyChoice == 1)
			{
				cout << "Tonegawa has drawn the Emperor Card!!!" << endl;
				cout << "Kaiji lost!" << "The drill is moving!!!!" << player.wager << "MM!!!" << endl;
				player.mm -= player.wager;
			}

			else
			{
				cout << "Tonegawa has drawn the Civillian Card!!!" << endl;
				cout << "IT'S A DRAW!!!! YOU'RE SAFE!!!" << endl;
				player.deckNames.pop_back();
				player.deckSize--;
			}

		}
	}

	else if (player.sideName == "Slave")
	{
		if (choice == 1)
		{
			cout << "Kaiji has DRAWN THE SLAVE CARD!!!" << endl;
			if (enemyChoice == 1)
			{
				cout << "Tonegawa has DRAWN THE EMPEROR CARD!!!" << endl;
				cout << "Kaiji Won!!!" << player.wager * 500000 << "!" << endl;
				player.money += (player.wager * 500000);
			}
			else
			{
				cout << "Tonegawa has drawn the Civillian " << endl;
				cout << "Kaiji LOST!!!! THE DRILL MOVES!!!! AHHHHH!!!! " << player.wager << "MM!!!!! " << endl;
				player.mm -= player.wager;
			}
		}

		else
		{
			cout << "Kaiji has drawn the Civillian Card!!! ";

			if (enemyChoice == 1)
			{
				cout << "Tonegawa has DRAWN THE EMPEROR CARD!!! " << endl;
				cout << "Kaiji LOST!!! THE DRILL MOVES!!!! AHHHHH!!! " << player.wager << "MM!!!" << endl;
				player.mm -= player.wager;
			}

			else
			{
				cout << "Tonegawa has drawn the Civillian" << endl;
				cout << "IT'S A DRAW!!!! " << endl;
				player.deckNames.pop_back();
				player.deckSize--;
			}
		}
	}

}

int main()
{
	srand(time(NULL));
	int round = 1; // Start of Round 1
	int choice;
	playerArsenal player, enemy;

	cout << "Emperor Card Midterms!!! " << endl;
	cout << "=========================== " << endl << endl;

	// Game will continue until the last round or if the game conditions are met
	while (round < 13)
	{
		// Play Round 
		printInfo(player);
		cout << "Round: " << round << "/12 " << endl;

		getWager(player);
		allDeck(player, enemy, round);

		system("pause");
		system("cls");

		do
		{
			printDeck(player);
			cout << endl << "Which card will you bet on??? " << endl;
			cin >> choice;
			playRound(player, enemy, choice);

			system("pause");
			system("cls");
		} while (choice != 1);

		player.deckNames.clear();
		enemy.deckNames.clear();
		// When round has ended. Increment Round 
		round++;

		system("cls");
	}
	cout << endl;

	system("pause");
	return 0;
}