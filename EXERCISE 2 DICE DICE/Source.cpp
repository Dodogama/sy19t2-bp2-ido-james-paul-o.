#include <iostream>
#include <string>
#include <time.h>

using namespace std;

struct dices
{
	int dice1;
	int dice2;
}player, AI;

//Betting Functions

int playerBet(int& bet, int& gold)
{
	//Input Bet

	while (bet <= 0 || bet > gold)
	{
		cout << "Please input your bet: ";
		cin >> bet;
	}
	gold -= bet;

	return bet;
}

void askPlayerBet(int& bet, int& gold)
{
	while (bet <= 0 || bet > gold)
	{
		cout << "Please input your bet: ";
		cin >> bet;
	}
	gold -= bet;
}

//Dice Functions
void diceRolls(int& diceOne, int& diceTwo)
{
	//DICE ROLL
	//variable = rand()% 
	diceOne = rand() % 6 + 1;
	diceTwo = rand() % 6 + 1;
}

//Payout Function
void payOuts(int playerBet, int& playerMoney , dices player , dices AI)
{
	int playerPayOut = player.dice1 + player.dice2;
	int aiPayOut = AI.dice1 + AI.dice2;

	if (playerPayOut == 2 || aiPayOut == 2)
	{
		if (playerPayOut == aiPayOut)
		{
			cout << "D R A W !!!" << endl;
			playerMoney += playerBet;
		}
		else if (playerPayOut == 2)
		{
			cout << "S N A K E  E Y E S" << endl;
			cout << "Player wins!" << playerBet << "GOLD" << endl;
			playerMoney	 += playerBet * 3;
		}
		else if (aiPayOut == 2)
		{
			cout << "S N A K E  E Y E S" << endl;
			cout << "AI wins!" << endl;
			cout << "YOU LOST" << playerBet << "GOLD!" << endl;
		}
	}
}

int playRound(int& playerMoney , int& playerBet)
{
	dices player, AI;

	cout << "player gold: " << playerMoney << endl;
	askPlayerBet(playerMoney , playerBet);
	cout << endl;
	diceRolls(player.dice1, player.dice2);
	diceRolls(AI.dice1, AI.dice2);
	payOuts(playerMoney, playerBet, player, AI);

	system("pause");
	return 0;
}


int main()
{
	int playerGold = 1000;
	int playerBet;

	srand(time(NULL));


	cout << "Dice Exercise basprog 2" << endl;
	cout << "==============" << endl << endl;

	//Play Round
	while (playerGold > 0)
	{
		playRound(playerGold, playerBet);
		playerBet = 0;
	}

	cout << "GGWP!!! YOU LOST!!!" << endl;

	system("pause");
	return 0;
}