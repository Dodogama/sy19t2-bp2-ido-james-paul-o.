#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;

//FUNCTIONS
struct items
{
	string name;
	int value;
};

int getRandomNumber()
{
	int y = rand() % 10 + 1;
	return y;
}

items getRandomGenerator()
{

	string nameRoster[20] =
	{
		"Health Potion"
		"Mana potion "
		"Stamina potion "
		"Antidote "
		"Greatsword "
		"Katana "
		"SOS! Flare"
		"Berserker Armor"
		"Dragonslayer"
		"Berserker Boots"
		"Teleporter"
		"Tranq Dart"
		"Silver Ring"
		"Black Robe"
		"Berserker Gloves"
		"Crossbow"
		"Lethal Dart"
		"Rock"
		"Dagger"
		"Bomb"
	};

	string randomName = nameRoster[rand() % 20];
	int value = rand() % 10 + 1;
	
	items randomItem = { randomName , value };

	return randomItem;
}

void printBackpack(items backpack[])
{
	for (int i = 0; i < 10; i++)
	{
		cout << backpack[i].name<< endl;
		cout << backpack[i].value << endl;
	}
}

int main()
{
	srand(time(NULL));

	items backPack[10];

	cout << "RANDOM ITEM GENERATOR!" << endl;
	cout << "====================== " << endl;

	for (int i = 0; i < 10; i++)
	{
		backPack[i] = getRandomGenerator();
	}
	
	printBackpack(backPack);


	system("pause");
	return 0;
}