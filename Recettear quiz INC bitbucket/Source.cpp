#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

struct Item
{
	string name;
	int gold;
	int customerPrice;
};

struct NPC
{
	string npcName;
	Item itemOrder;
};

//Generate items
vector<Item> generateItem(vector<Item> inventory)
{
	for (int i = 0; i < 10; i++)
	{
		inventory.push_back(Item());
	}

	inventory[0].name = "Wooden Armband";
	inventory[0].gold = 15;
	inventory[0].customerPrice = 23;

	inventory[1].name = "Leather Glove";
	inventory[1].gold = 20;
	inventory[1].customerPrice = 33;

	inventory[2].name = "Wool Hat";
	inventory[2].gold = 25;
	inventory[3].customerPrice = 28;

	inventory[3].name = "Focus Staff";
	inventory[3].gold = 30;
	inventory[3].customerPrice = 55;

	inventory[4].name = "Wooden Shield";
	inventory[4].gold = 35;
	inventory[4].customerPrice = 62;

	inventory[5].name = "LongSword";
	inventory[5].gold = 120;
	inventory[5].customerPrice = 122;

	inventory[6].name = "Crafter's Knife";
	inventory[6].gold = 140;
	inventory[6].customerPrice = 272;

	inventory[7].name = "Longbow";
	inventory[7].gold = 140;
	inventory[7].customerPrice = 186;

	inventory[8].name = "Willpower Ring";
	inventory[8].gold = 600;
	inventory[8].customerPrice = 1083;

	inventory[9].name = "Knight's Helm";
	inventory[9].gold = 900;
	inventory[9].customerPrice = 1596;


	return inventory;
}

// Printing the Items and their Values
void displayItems(vector<Item> inventory)
{
	for (int i = 0; i < inventory.size(); i++) 
	{
		cout << i + 1 << ")";
		cout << inventory[i].name << "|" << "Value:" << inventory[i].gold << endl;
	}
	
}

//Print player Inventory
void printPlayerInventory(vector<Item> personalInventory)
{
	cout << "Player Inventory" << endl;

	if (personalInventory.size() < 1)
	{
		cout << "EMPTY!!!" << endl;
	}
	else
	{
		for (int i = 0; i < personalInventory.size(); i++)
		{
			cout << personalInventory[i].name << endl;
		}
	}
}

//Buying option for the player
vector<Item> buyItem(vector<Item> inventory , vector<Item> personalInventory, int& gold)
{
	int choice;
	Item myChoice;
	do
	{
		cout << "Current Gold: " << gold << endl;
		printPlayerInventory(personalInventory);

		cout << "==========================" << endl;

		displayItems(inventory);
		cout << "Enter an Item to buy (0 to quit) " << ":" << endl;
		cin >> choice;

		if (choice != 0)
		{
			if (gold < inventory[choice - 1].gold)
			{
				cout << " YOU CANNOT AFFORD THIS " << endl;
			}
			else
			{
				myChoice = { inventory[choice - 1].name, inventory[choice - 1].gold };
				personalInventory.push_back(myChoice);
				gold -= inventory[choice - 1].gold;

			}
			
		}
		

		system("cls");
		
	} while (choice != 0);
	
	return personalInventory;
}

// random NPC generator
vector<NPC> generateNpc(vector<NPC> npc)
{
	for (int i = 0; i < 5; i++)
	{
		npc.push_back(NPC());
	}

	npc[0].npcName = "Man";

	npc[1].npcName = "Old Man";

	npc[2].npcName = "Woman";

	npc[3].npcName = "Girl";

	return npc;
}

// Npc order function
vector<Item> npcOrder(vector<NPC> buyer, vector<Item> inventory, vector<Item> personalInventory, int& gold)
{
	//Generates the customers for the day
	for (int i = 0; i < 5; i++)
	{
		cout << "Begin Shopping!!! " << endl;
		printPlayerInventory(personalInventory);
		cout << "Current Gold: " << gold << endl;

		//Checks if the item is bought
		bool isbought = false;

		//Random Npc for the Day
		int randomPerson = rand() % 4;
		//Random Item Order
		int randomItem = rand() % 10;
		buyer[randomPerson].itemOrder = inventory[randomItem];
		cout << buyer[randomPerson].npcName << " is looking for " << buyer[randomPerson].itemOrder.name << endl;
		
		//Item is being searched
		for (int j = 0; j < personalInventory.size(); j++)
		{
			
			//Item is sold
			if(buyer[randomPerson].itemOrder.name == personalInventory[j].name)
			{
				
				cout << buyer[randomPerson].itemOrder.name << " Is SOLD! " << endl;
				gold += buyer[randomPerson].itemOrder.customerPrice;
				personalInventory.erase(personalInventory.begin() + j);
				isbought = true;
				break;
			}
		
		}
		if (isbought == false)
		{
			cout << "Customer left the store T___T " << endl;
		}

		system("pause");
		system("cls");
		
	}

	return personalInventory;
}


int main()
{
	srand(time(NULL));

	int playerGold = 1000;
	vector<Item> inventories;
	vector<Item> personalInventory;
	vector<NPC> people;
	

	cout << "Quiz # 1  Basprog 2" << endl;
	cout << "========================" << endl;
	cout << "Current Gold" << ":" << playerGold << endl << endl;
	cout << "GUILD ITEMS" << endl;
	cout << "=========================" << endl;

	//Game
	cout << "Player Gold: " << playerGold << endl;
	inventories = generateItem(inventories);
	people = generateNpc(people);
	
	while (playerGold < 10000)
	{
		personalInventory = buyItem(inventories, personalInventory, playerGold);
		personalInventory = npcOrder(people, inventories, personalInventory, playerGold);


		system("pause");
		system("cls");
	}

	cout << " YOU WIN!!! YOU'VE REACHED THE TARGET AMOUNT!!!! " << endl;
	
	system("pause");
	return 0;
}