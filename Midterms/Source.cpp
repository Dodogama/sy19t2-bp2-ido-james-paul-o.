#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <vector>

using namespace std;

struct playerItem
{
	string sideName;
	vector <string> deckNames;
	int deckSize = 5;
	int money = 0;
	int wager = 0;
	int mm = 30;
	
};

void displayInfo(playerItem& player)
{
	cout << "Cash: " << player.money << endl;
	cout << "MM LEFT: " << player.mm << endl;
}

void getWager(playerItem& player)
{
	do
	{
		cout << "how much do you want to wager? " << endl;
		cin >> player.wager;
	} while (player.wager <= 0 || player.wager > player.mm);
}

void deckChecker(playerItem& player, playerItem& enemy, int& playRound)
{
	if ((playRound >= 1 && playRound <= 3) || (playRound >= 7 && playRound <= 9))
	{
		player.sideName = "Emperor Card!";
		player.deckNames.push_back("Emperor Card!");
		for (int i = 0; i < 4; i++)
		{
			player.deckNames.push_back("Citizen");
		}

		enemy.sideName = "Slave";
		enemy.deckNames.push_back("Slave");
		for (int i = 0; i < 4; i++)
		{
			enemy.deckNames.push_back("Citizen");
		}
	}

	else if ((playRound >= 4 && playRound <= 6 || (playRound >= 10 && playRound <= 12)))
	{
		player.sideName = "Emperor Card!";
		player.deckNames.push_back("Emperor Card!");
		for (int i = 0; i < 4; i++)
		{
			player.deckNames.push_back("Citizen");
		}

		enemy.sideName = "Slave Card!";
		enemy.deckNames.push_back("Slave Card!");
		for (int i = 0; i < 4; i++)
		{
			enemy.deckNames.push_back("Citizen");
		}
	}

}

void printDeck(playerItem& player)
{
	cout << "Kaiji's Side: " << player.sideName << endl;
	for (int i = 0; i < player.deckSize; i++)
	{
		cout << "[" << i + 1 << "]" << player.deckNames[i] << endl;
	}
}

void gameRound(playerItem& player , playerItem& enemy , int& choice)
{
	int enemyChoice = rand() % 5 + 1;

	if (player.sideName == "Emperor")
	{
		if (choice == 1)
		{
			cout << "Kaiji has DRAWN THE EMPEROR CARD!!! " << endl;
			if (enemyChoice > 1)
			{
				cout << "Tonegawa has drawn a Civillian Card!" << endl;
				cout << "KAIJI WON!!! " << player.wager * 100000 << "YENNIEEEE!!! " << endl;
				player.money += (player.wager * 100000);
			}

			else
			{
				cout << "Tonegawa has drawn the SLAVE CARD!" << endl;
				cout << "Kaiji Lost! OH NO! THE DRILL STARTS MOVING!!!" << player.wager << "MM!!!!!" << endl;
				player.mm -= player.wager;
			}
		}

		else
		{
			cout << "Kaiji has drawn the Civillian card!" << endl;

			if (enemyChoice == 1)
			{
				cout << "Tonegawa has drawn the Emperor Card!!!" << endl;
				cout << "Kaiji lost!" << "The drill is moving!!!!" << player.wager << "MM!!!" << endl;
				player.mm -= player.wager;
			}

			else
			{
				cout << "Tonegawa has drawn the Civillian Card!!!" << endl;
				cout << "IT'S A DRAW!!!! YOU'RE SAFE!!!" << endl;
				player.deckNames.pop_back();
				player.deckSize--;
			}

		}
	}

	else if (player.sideName == "Slave")
	{
		if (choice == 1)
		{
			cout << "Kaiji has DRAWN THE SLAVE CARD!!!" << endl;
			if (enemyChoice == 1)
			{
				cout << "Tonegawa has DRAWN THE EMPEROR CARD!!!" << endl;
				cout << "Kaiji Won!!!" << player.wager * 500000 << "!" << endl;
				player.money += (player.wager * 500000);
			}
			else
			{
				cout << "Tonegawa has drawn the Civillian " << endl;
				cout << "Kaiji LOST!!!! THE DRILL MOVES!!!! AHHHHH!!!! " << player.wager << "MM!!!!! " << endl;
				player.mm -= player.wager;
			}
		}

		else
		{
			cout << "Kaiji has drawn the Civillian Card!!! ";

			if (enemyChoice == 1)
			{
				cout << "Tonegawa has DRAWN THE EMPEROR CARD!!! " << endl;
				cout << "Kaiji LOST!!! THE DRILL MOVES!!!! AHHHHH!!! " << player.wager << "MM!!!" << endl;
				player.mm -= player.wager;
			}

			else
			{
				cout << "Tonegawa has drawn the Civillian" << endl;
				cout << "IT'S A DRAW!!!! " << endl;
				player.deckNames.pop_back();
				player.deckSize--;
			}
		}
	}
}

int main()
{
	srand(time(NULL));
	int playRound;
	int choice;
	playerItem player, enemy;
	int round = 1;

	cout << "MIDTERMS IDO EMPEROR CARD!" << endl;
	cout << "=====================================" << endl << endl;

	while (round < 13)
	{
		displayInfo(player);
		cout << "Round: " << round << "/12 " << endl;

		getWager(player);
		deckChecker(player, enemy, round);

		system("pause");
		system("cls");

		do
		{
			printDeck(player);
			cout << endl << "What card will you bet on: " << endl;
			cin >> choice;
			gameRound(player, enemy, choice);


			system("pause");
			system("cls");
		} while (choice != 1);

		player.deckNames.clear();
		enemy.deckNames.clear();
		round++;

		system("cls");
	}	

	system("pause");
	return 0;
}